# Getting started

1.  `npm start` To run the app in the development mode . Browse to [http://localhost:3000](http://localhost:3000) to view it in the browser. The page will reload if you make edits. You will also see any lint errors in the console.

2.  `npm test` Launches the test runner in the watch mode. It will run the following -
    a) unit tests - For actions, reducers and utils using jest.
    b) snapshot tests - For structural testing components using storyshot addon of storybook.

3.  `npm run build` Builds the app for production to the `build` folder.
